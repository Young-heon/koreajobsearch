package koJobSearcher;

import koJobSearcher.toExcel.ExportExcel;

public class Main {
	
	public static void main(String [] args){
		String path = System.getProperty("user.home")+"/Desktop";
		
		long start = System.currentTimeMillis();
				
		ExportExcel ee = new ExportExcel();
		ee.ExcelWriter(path);
		long end = System.currentTimeMillis();
		System.out.println("Execute Time : "+(end-start)/1000.0);
	}
}
