package koJobSearcher.collect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Carrer {
	
	public String getHtml(){
		long start = System.currentTimeMillis();
		URL url; // The URL to read
        HttpURLConnection conn; // The actual connection to the web page
        BufferedReader rd; // Used to read results from the web page
        String line; // An individual line of the web page HTML
        String result = ""; // A long string containing all the HTML
        String connectUrl = "http://www.career.co.kr/jobs/junior.asp?page=";
        try {
        	for(int i=1;i<4;i++){
        		
        	
           url = new URL(connectUrl+i);
           conn = (HttpURLConnection) url.openConnection();
           conn.setRequestMethod("GET");
           
           rd = new BufferedReader(new InputStreamReader(conn.getInputStream(),"EUC-KR"));
           while ((line = rd.readLine()) != null) {
              result += line;
           }
           rd.close();
        	}
        } catch (Exception e) {
           e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("Carrer Parsing time : "+(end-start)/1000 +"s");
        return result;
		
	}
	

	public ArrayList<TreeMap<String,String>> connect(){
		TreeMap<String,String> map=null ;
		ArrayList<TreeMap<String,String>> list = new ArrayList<TreeMap<String,String>>();
		
		//for(int i=1;i<4;i++)
		//{
		//Document doc = Jsoup.connect("http://www.career.co.kr/jobs/junior.asp?page="+i).get();
		Document doc = Jsoup.parse(getHtml());
		Elements line = doc.select("#career_contents > form > table > tbody > tr");
		for(Element item : line){

			map = new TreeMap<String,String>();
			map.put("1companyName", item.select(".subject").text());
			map.put("2Subject", item.select(".check").text());
			map.put("4dueDate", item.select(".tc").text());
			map.put("5link", "http://www.career.co.kr"+item.select(".check").attr("href"));
			map.put("3condition",item.select(".info").text());
			
			list.add(map);
		//}
		}
		return list;
	}

		
}
