package koJobSearcher.collect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Incruit {
	public String getHtml(){
		long start = System.currentTimeMillis();
		URL url; // The URL to read
        HttpURLConnection conn; // The actual connection to the web page
        BufferedReader rd; // Used to read results from the web page
        String line; // An individual line of the web page HTML
        String result = ""; // A long string containing all the HTML
        String saraminUrl = "http://job.incruit.com/entry/searchjob.asp?ct=12&ty=1&cd=1&page=1";
        try {
        	
           url = new URL(saraminUrl);
           conn = (HttpURLConnection) url.openConnection();
           conn.setRequestMethod("GET");
           
           rd = new BufferedReader(new InputStreamReader(conn.getInputStream(),"EUC-KR"));
           while ((line = rd.readLine()) != null) {
              result += line;
           }
           rd.close();
        } catch (Exception e) {
           e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("Incruit Parsing time : "+(end-start)/1000 +"s");
        return result;
		
	}
	

	public ArrayList<TreeMap<String,String>> connect(){
		
		TreeMap<String,String> map=null ;
		ArrayList<TreeMap<String,String>> list = new ArrayList<TreeMap<String,String>>();
		
		//			Document doc 
		//			= Jsoup.connect("http://job.incruit.com/entry/searchjob.asp?ct=12&ty=1&cd=1&page=1").get();
					Document doc = Jsoup.parse(getHtml());
					Elements line = doc.select("#list > tbody > tr");
					
					for(Element item : line){
						
						map = new TreeMap<String,String>();
						map.put("1companyName", item.select(".company").text());
						map.put("2Subject", item.select(".vcheck").text());
						map.put("6dueDate", item.select(".date").text());
						map.put("7link", item.select(".vcheck").attr("href"));
						map.put("5region",item.select(".region").text());
						map.put("4finalSchool",item.select(".edu").text());
						map.put("3condition",item.select(".conditon").text());
						
						list.add(map);
						
						
						//System.out.println(item.select(".company").text());
		//				System.out.println(item.select(".vcheck").text());
		//				System.out.println(item.select(".vcheck").attr("href"));
		//				System.out.println(item.select(".condition").text());
		//				
		//				System.out.println(item.select(".edu").text());
		//				System.out.println(item.select(".region").text());
		//				System.out.println(item.select(".date").text());
		//				
		//				System.out.println();
						
					}
		return list;
	}
}
