package koJobSearcher.collect;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class JobKorea {
	
	String jobKoreaUrl = "http://www.jobkorea.co.kr/Include/Starter/Recruit/List.asp?Page=1&ListGbn=New&RecordCnt=1151&PageCount=58&C_Name=&GI_Job_Gbn=&OrderBy=GI_W_Date&PageSize=60";

	 public String getHTML() {
         URL url; // The URL to read
         HttpURLConnection conn; // The actual connection to the web page
         BufferedReader rd; // Used to read results from the web page
         String line; // An individual line of the web page HTML
         String result = ""; // A long string containing all the HTML
         try {
            url = new URL(jobKoreaUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream(),"EUC-KR"));
            while ((line = rd.readLine()) != null) {
               result += line;
            }
            rd.close();
         } catch (Exception e) {
            e.printStackTrace();
         }
         String html ="<html><head></head><body><table>";
         html+=result;
         html+="</table></body></html>";
         return html;
      }
	
	
	public ArrayList<TreeMap<String,String>> connect(){
		
		TreeMap<String,String> map=null ;
		ArrayList<TreeMap<String,String>> list = new ArrayList<TreeMap<String,String>>();
		
		
		Document doc = Jsoup.parse(getHTML());
		Elements items = doc.select("tr");
		
		for(Element item : items){
			
//			String urlWork = item.select(".title").html().substring(12,65);
//			System.out.println(item.select(".coName").text() +"  "+ item.select(".part").text()+"  "+item.select(".title").text()+"  "+item.select(".day").text());
//			System.out.println(urlWork);
//			
			map = new TreeMap<String,String>();
			map.put("1companyName", item.select(".coName").text() +"  "+ item.select(".part").text());
			map.put("2Subject", item.select(".title").text());
			map.put("4dueDate", item.select(".day").text());
			map.put("3link", "http://www.jobkorea.co.kr"+item.select(".title").html().substring(12,65));
			
			list.add(map);
		}
	
		return list;
	}
	
	
}
