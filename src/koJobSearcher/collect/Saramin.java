package koJobSearcher.collect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Saramin {
	
	public String getHtml(){
		long start = System.currentTimeMillis();
		URL url; // The URL to read
        HttpURLConnection conn; // The actual connection to the web page
        BufferedReader rd; // Used to read results from the web page
        String line; // An individual line of the web page HTML
        String result = ""; // A long string containing all the HTML
        String saraminUrl = "http://www.saramin.co.kr/recruit/RecruitBbsSearch.php?code=bbs_recruit&pannel=org&career%5B%5D=1&careerlimit1=0&careerlimit2=0&career%5B%5D=0&kwd=#bbs-list-top";
        try {
        	
           url = new URL(saraminUrl);
           conn = (HttpURLConnection) url.openConnection();
           conn.setRequestMethod("GET");
           
           rd = new BufferedReader(new InputStreamReader(conn.getInputStream(),"EUC-KR"));
           while ((line = rd.readLine()) != null) {
              result += line;
           }
           rd.close();
        } catch (Exception e) {
           e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("Saramin Parsing time : "+(end-start)/1000 +"s");
        return result;
		
	}
	
	

	public ArrayList<TreeMap<String,String>> connect(){
		
		TreeMap<String,String> map=null ;
		ArrayList<TreeMap<String,String>> list = new ArrayList<TreeMap<String,String>>();
		Document doc = Jsoup.parse(getHtml());
		Elements itemsRow = doc.select(".item-row");
		
		
		for(Element itemRow : itemsRow){
			
			map = new TreeMap<String,String>();
			map.put("1companyName", itemRow.select("td.company_nm").text());
			map.put("2Subject", itemRow.select(".job-title").text());
			map.put("3dueDate", itemRow.select(".closing-date").text());
			map.put("4link", "http://www.saramin.co.kr"+itemRow.select(".title").attr("href"));
			
			list.add(map);
			//System.out.println(itemRow.select("td.company_nm").text()+" "+itemRow.select(".job-title").text() +" 마감일 :  "+itemRow.select(".closing-date").text());
			//System.out.println(itemRow.select(".title").attr("href"));
			//System.out.println("====================================");
		}
		return list;
	}
	
}
