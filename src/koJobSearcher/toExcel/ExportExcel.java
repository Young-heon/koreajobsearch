package koJobSearcher.toExcel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

import koJobSearcher.collect.Carrer;
import koJobSearcher.collect.Incruit;
import koJobSearcher.collect.JobKorea;
import koJobSearcher.collect.Saramin;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExportExcel {
	XSSFWorkbook workbook = new XSSFWorkbook();
	ArrayList<XSSFSheet> sheetlist = null;
	

	public void SaraminOutput(){
		
		XSSFSheet saraminSheet = workbook.createSheet("사람인");
		Saramin s = new Saramin();
		ArrayList<TreeMap<String,String>> list = s.connect();
		ArrayList<String> columnList = new ArrayList<String>();
		
		addColumnList(list, columnList);
		addSheet(saraminSheet, list, columnList);
		
	}

	public void CarrerOutput(){
		//long start = System.currentTimeMillis();
		XSSFSheet carrerSheet = workbook.createSheet("커리어");
		Carrer s= new Carrer();
		ArrayList<TreeMap<String,String>> list = s.connect();
		ArrayList<String> columnList = new ArrayList<String>();
		
		addColumnList(list, columnList);
		addSheet(carrerSheet, list, columnList);
		//long end = System.currentTimeMillis();
		//System.out.println("Carrer Parsing Time : "+(end-start)/1000+"s");
		
	}
	
	public void JobkoreaOutput(){
		long start = System.currentTimeMillis();
		XSSFSheet jobkoreaSheet = workbook.createSheet("잡코리아");
		JobKorea s= new JobKorea();
		ArrayList<TreeMap<String,String>> list = s.connect();
		ArrayList<String> columnList = new ArrayList<String>();
		
		addColumnList(list, columnList);
		addSheet(jobkoreaSheet, list, columnList);
		long end = System.currentTimeMillis();
		System.out.println("Carrer Parsing Time : "+(end-start)/1000+"s");
	}
	
	public void IncruitOutput(){
		XSSFSheet incruitSheet = workbook.createSheet("인크루트");
		Incruit i = new Incruit();
		ArrayList<TreeMap<String,String>> list = i.connect();
		ArrayList<String> columnList = new ArrayList<String>();
		
		addColumnList(list, columnList);
		addSheet(incruitSheet, list, columnList);
		
	}
	
	public void ExcelWriter(String fileOutputPath){
		
		FileOutputStream fos = null;
		
		SaraminOutput();
		CarrerOutput();
		IncruitOutput();
		JobkoreaOutput();
		
		try {
			fos = new FileOutputStream(fileOutputPath+"/OutputData.xlsx");
			workbook.write(fos);
			fos.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addColumnList(ArrayList<TreeMap<String, String>> list, ArrayList<String> columnList) {
		if(list!=null&&list.size()>0){
			TreeMap<String,String> m = list.get(0);
			for(String str:m.keySet()){
				columnList.add(str);
			}
		}
	}
	
	public void addSheet(XSSFSheet sheet,
		ArrayList<TreeMap<String, String>> list,ArrayList<String> columnList) {
		
		XSSFRow row;
		XSSFCell cell;
		String cellContent = "";
		CreationHelper creationHelper = workbook.getCreationHelper();
		XSSFFont hlinkfont = workbook.createFont();
		XSSFCellStyle hlinkstyle = workbook.createCellStyle();
	      hlinkfont.setUnderline(XSSFFont.U_SINGLE);
	      hlinkfont.setColor(HSSFColor.BLUE.index);
	      hlinkstyle.setFont(hlinkfont);
		if(list!=null&&list.size()>0){
			int i=0;
			for(TreeMap<String,String> hs : list){
				
			row = sheet.createRow((short)i);
			i++;
			if(columnList!=null && columnList.size()>0){
				for(int j=0;j<columnList.size();j++){
					cell = row.createCell(j);
					cellContent = String.valueOf(hs.get(columnList.get(j)));
					System.out.println(cellContent);
					if(cellContent.length() > 4 &&cellContent.substring(0, 4).equals("http")){
						XSSFHyperlink link = (XSSFHyperlink)creationHelper
							      .createHyperlink(Hyperlink.LINK_URL);
							      link.setAddress(cellContent.replaceAll("&amp;", "&").trim());
							      cell.setHyperlink((XSSFHyperlink) link);
							      cell.setCellStyle(hlinkstyle);
						
						cell.setCellValue(cellContent);
					}else{
						cell.setCellValue(cellContent);
					}
					sheet.autoSizeColumn((short)j);
					 sheet.setColumnWidth(j, (sheet.getColumnWidth(j))+512 );
				}
			}
			
			}
		}
	}
	
}
